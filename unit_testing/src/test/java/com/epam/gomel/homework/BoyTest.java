package com.epam.gomel.homework;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.time.Month;

import static org.testng.Assert.*;

public class BoyTest extends BaseTest {

    @Test(dataProvider = "summerMonth")
    public void testIsSummerMonth(Boy boy) {
        assertTrue(boy.isSummerMonth());
    }

    @Test(dataProvider = "notSummerMonth")
    public void testIsNotSummer(Boy boy) {
        assertFalse(boy.isSummerMonth());
    }

    @Parameters("rich")
    @Test
    public void testIsRich(double wealth) {
        boy = new Boy(Month.JULY, wealth);
        assertTrue(boy.isRich());
    }

    @Parameters("poor")
    @Test
    public void testIsNotRich(double wealth) {
        boy = new Boy(Month.JULY, wealth);
        assertFalse(boy.isRich());
    }

    @Test
    public void testIsWealthIsGreater() {
        boy = new Boy(Month.JULY, 1000000.1);
        assertTrue(boy.isRich());
    }

    @Test(dataProvider = "money")
    public void testSpendSomeMoney(double wealth, double amountForSpending, double expected) {
        boy = new Boy(Month.JUNE, wealth);
        boy.spendSomeMoney(amountForSpending);
        assertEquals(boy.getWealth(), expected);
    }

    @Test(expectedExceptions = RuntimeException.class)
    public void testMessageWhenHugeAmount() {
        boy = new Boy(Month.APRIL, 1000000);
        girl = new Girl(true, true, boy);
        girl.spendBoyFriendMoney(boy.getWealth() + 1);
    }

    @Test
    public void testIsPrettyGirlFriend() {
        girl = new Girl(true);
        boy = new Boy(Month.AUGUST, 1000000, girl);
        assertTrue(boy.isPrettyGirlFriend());
    }

    @Test
    public void testIsUglyGirlFriend() {
        boy = new Boy(Month.AUGUST, 1000000, girl);
        assertFalse(boy.isPrettyGirlFriend());
    }

    @Test
    public void testIsPrettyWhenNotExists() {
        assertFalse(boy.isPrettyGirlFriend());
    }

    @Test
    public void testIsGirlfriendExists() {
        boy = new Boy(Month.AUGUST, 1000000, girl);
        assertSame(boy.getGirlFriend(), girl);
    }

    @Test
    public void testIsGirlfriendNotExists() {
        assertNull(boy.getGirlFriend());
    }

    @Test
    public void testGetMoodExcellent() {
        girl = new Girl(true);
        boy = new Boy(Month.AUGUST, 1000000, girl);
        assertEquals(boy.getMood(), Mood.EXCELLENT);
    }

    @Test
    public void testGetMoodGood() {
        girl = new Girl(true);
        boy = new Boy(Month.APRIL, 1000000, girl);
        assertEquals(boy.getMood(), Mood.GOOD);
    }

    @Test
    public void testGetMoodNeutral() {
        boy = new Boy(Month.JULY, 1000000);
        assertEquals(boy.getMood(), Mood.NEUTRAL);
    }

    @Test
    public void testGetMoodBadIsRich() {
        boy = new Boy(Month.APRIL, 1000000);
        assertEquals(boy.getMood(), Mood.BAD);
    }

    @Test
    public void testGetMoodBadIsSummer() {
        boy = new Boy(Month.JUNE);
        assertEquals(boy.getMood(), Mood.BAD);
    }

    @Test
    public void testGetMoodBadIdPrettyGirlfriend() {
        girl = new Girl(true);
        boy = new Boy(Month.APRIL, 0 , girl);
        assertEquals(boy.getMood(), Mood.BAD);
    }

    @Test
    public void testGetMoodHorrible() {
        boy = new Boy(Month.APRIL, 0 , girl);
        assertEquals(boy.getMood(), Mood.HORRIBLE);
    }

    @Test
    public void testGetMoodHorribleWithoutGirlfriend() {
        boy = new Boy(Month.APRIL, 0);
        assertEquals(boy.getMood(), Mood.HORRIBLE);
    }

}