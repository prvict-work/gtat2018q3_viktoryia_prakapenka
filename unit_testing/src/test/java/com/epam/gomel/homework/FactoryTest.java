package com.epam.gomel.homework;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import java.time.Month;

public class FactoryTest {

    @DataProvider(name = "factoryProvider")
    public Object[][] factoryDataProvide() {
        return new Object[][]{
                {Month.AUGUST, 1000000, new Girl(true, false)},
                {Month.JULY, 1000000.1, new Girl(true, true)},
                {Month.JUNE, 5000000, new Girl(true, false)},
        };
    }

    @Factory(dataProvider = "factoryProvider")
    public Object[] createTest(Month month, double wealth, Girl girl) {
        System.out.println(String.format("Generate test for boy: %s %s with girl %s %s",
                month, wealth, girl.isPretty(), girl.isSlimFriendGotAFewKilos()));
        return new Object[] {new FactoryBasedTest( month, wealth, girl)};
    }
}
