package com.epam.gomel.homework.runners;

import com.epam.gomel.homework.listeners.TestListener;
import org.testng.TestNG;

import java.util.Arrays;
import java.util.List;

public class TestRunner {

    public static void main(String[] args) {
        TestNG testNG = new TestNG();
        testNG.addListener(new TestListener());
        List<String> suites = Arrays.asList(
                "testng.xml",
                "groups_testng.xml," +
                        "factory_testng.xml");
        testNG.setTestSuites(suites);
        testNG.run();
    }
}
