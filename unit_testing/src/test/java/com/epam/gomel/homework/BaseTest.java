package com.epam.gomel.homework;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;

import java.time.Month;

public class BaseTest {

    Girl girl;
    Boy boy;
    Month month;
    double wealth;

    @BeforeMethod
    public void setUp() {
        girl = new Girl();
        boy = new Boy(Month.JUNE);
    }

    @DataProvider
    public Object[][] summerMonth() {
        return new Object[][]{
                {new Boy(Month.JUNE)},
                {new Boy(Month.JULY)},
                {new Boy(Month.AUGUST)},
        };
    }

    @DataProvider
    public Object[][] notSummerMonth() {
        return new Object[][]{
                {new Boy(Month.JANUARY)},
                {new Boy(Month.FEBRUARY)},
                {new Boy(Month.MARCH)},
                {new Boy(Month.APRIL)},
                {new Boy(Month.MAY)},
                {new Boy(Month.SEPTEMBER)},
                {new Boy(Month.OCTOBER)},
                {new Boy(Month.NOVEMBER)},
                {new Boy(Month.DECEMBER)},
        };
    }

    @DataProvider
    public Object[][] money() {
        return new Object[][]{
                {1000000.0, 100, 999900.0},
                {1000000.0, 1000000, 0.0},
                {1000000.0, 0, 1000000.0},
        };
    }
}
