package com.epam.gomel.homework;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Month;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class FactoryBasedTest extends BaseTest {


    public FactoryBasedTest(Month month, double wealth, Girl girl) {
        this.month = month;
        this.wealth = wealth;
        this.girl = girl;
    }

    @BeforeMethod
    public void setUp() {
        boy = new Boy(month, wealth, girl);
        girl = new Girl(true, true, boy);
    }

    @Test
    public void testIsBoyfriendRich() {
        assertTrue(girl.isBoyfriendRich());
    }

    @Test(dependsOnMethods = "testIsBoyfriendRich")
    public void testGetBoyMoodExcellent() {
        assertEquals(boy.getMood(), Mood.EXCELLENT);
    }

    @Test
    public void testIsSummerMonth() {
        assertTrue(boy.isSummerMonth());
    }

    @Test(dependsOnMethods = "testIsBoyfriendRich", alwaysRun = true)
    public void testIsReachBoyFriendBuyNewShoesForPrettyGirl() {
        assertTrue(girl.isBoyFriendWillBuyNewShoes());
    }
}
