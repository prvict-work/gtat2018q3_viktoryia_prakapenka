package com.epam.gomel.homework;

import static org.hamcrest.Matchers.*;
import static org.testng.Assert.*;
import static org.hamcrest.MatcherAssert.*;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.time.Month;


import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;


public class GirlTest extends BaseTest {

    @Test(groups={"smoke", "negative"})
    public void testIsNotPretty() {
        assertThat(girl.isPretty(), is(not(equalTo(true))));
    }

    @Test(groups = {"smoke"}, priority = 3)
    public void testIsPretty() {
        girl = new Girl(true);
        assertThat(girl.isPretty(), is(equalTo(true)));
    }

    @Test(groups = {"smoke"}, priority = 1)
    public void testIsBoyfriendExists() {
        girl = new Girl(true,true, boy);
        assertThat(girl.getBoyFriend(), is(equalTo(boy)));
    }

    @Test
    public void testIsBoyfriendNotExists() {
        assertThat(girl.getBoyFriend(), nullValue());
    }

    @Parameters("rich")
    @Test
    public void testIsBoyfriendRich(double wealth) {
        boy = new Boy(Month.APRIL, wealth);
        girl = new Girl(true, true, boy);
        assertTrue(girl.isBoyfriendRich());
    }

    @Test
    public void testIsBoyfriendPoor() {
        girl = new Girl(true, true, boy);
        assertFalse(girl.isBoyfriendRich());
    }

    @Test
    public void testIsBoyfriendRichIfNull() {
        assertFalse(girl.isBoyfriendRich());
    }

    @Parameters("poor")
    @Test
    public void testIsBoyfriendPoorBoundaryLess(double wealth) {
        boy = new Boy(Month.APRIL, wealth);
        girl = new Girl(true,true, boy);
        assertFalse(girl.isBoyfriendRich());
    }

    @Test
    public void testIsBoyfriendRichBoundaryGreater() {
        boy = new Boy(Month.APRIL, 1000000.1);
        girl = new Girl(true,true, boy);
        assertTrue(girl.isBoyfriendRich());
    }

    @Test
    public void testIsBoyfriendPoorBoundaryNegative() {
        boy = new Boy(Month.APRIL, -1);
        System.out.println(boy.getWealth());
        girl = new Girl(true,true, boy);
        assertFalse(girl.isBoyfriendRich());
    }

    @Test
    public void testIsBoyfriendRichLetter() {
        boy = new Boy(Month.APRIL, 's');
        System.out.println(boy.getWealth());
        girl = new Girl(true,true, boy);
        assertFalse(girl.isBoyfriendRich());
    }


    @Test
    public void testIsSpendMoneyInvoked() {
        Boy mockBoy = spy(new Boy(Month.APRIL, 1000000));
        girl = new Girl(true, true, mockBoy);
        girl.spendBoyFriendMoney(100);
        verify(mockBoy).spendSomeMoney(100);
    }

    @Test
    public void testIsRichInvoked() {
        Boy mockBoy = spy(new Boy(Month.APRIL, 1000000));
        girl = new Girl(true, true, mockBoy);
        girl.spendBoyFriendMoney(100);
        verify(mockBoy).isRich();
    }

    @Test(dataProvider = "money")
    public void testSpendBoyfriendMoney(double wealth, double amountForSpending, double expected) {
        boy = new Boy(Month.APRIL, wealth);
        girl = new Girl(true, true, boy);
        girl.spendBoyFriendMoney(amountForSpending);
        assertEquals(boy.getWealth(),expected);
    }

    @Test(groups = {"smoke"}, priority = 2)
    public void testIsReachBoyFriendBuyNewShoesForPrettyGirl() {
        boy = new Boy(Month.APRIL, 1000000);
        girl = new Girl(true, true, boy);
        assertTrue(girl.isBoyFriendWillBuyNewShoes());
    }

    @Test
    public void testIsReachBoyFriendBuyShoesForUglyGirl() {
        boy = new Boy(Month.APRIL, 1000000);
        girl = new Girl(false, true, boy);
        assertFalse(girl.isBoyFriendWillBuyNewShoes());
    }

    @Test
    public void testIsPoorBoyFriendBuyShoesForPrettyGirl() {
        girl = new Girl(true, true, boy);
        assertFalse(girl.isBoyFriendWillBuyNewShoes());
    }

    @Test
    public void testIsPoorBoyFriendBuyShoesForUglyGirl() {
        girl = new Girl(false, true, boy);
        assertFalse(girl.isBoyFriendWillBuyNewShoes());
    }

    @Test
    public void testIsSlimFriendBecameFatWhenGirlIsUgly() {
        assertTrue(girl.isSlimFriendBecameFat());
    }

    @Test
    public void testIsSlimFriendBecameFatWhenGirlIsPretty() {
        girl = new Girl(true, true);
        assertFalse(girl.isSlimFriendBecameFat());
    }

    @Test(groups = {"smoke"}, priority = 0)
    public void testIsSlimFriendBecameFatWithoutKilosWhenGirlIsPretty() {
        girl = new Girl(true, false);
        assertFalse(girl.isSlimFriendBecameFat());
    }

    @Test
    public void testIsSlimFriendBecameFatWithoutKilosWhenGirlIsUgly() {
        girl = new Girl(false, false);
        assertFalse(girl.isSlimFriendBecameFat());
    }

    @Test
    public void testGirlExcellentMood() {
        boy = new Boy(Month.APRIL, 1000000);
        girl = new Girl(true, true, boy);
        assertEquals(girl.getMood(), Mood.EXCELLENT);
    }

    @Test
    public void testGirlGoodMoodWhenPretty() {
        girl = new Girl(true);
        assertEquals(girl.getMood(), Mood.GOOD);
    }

    @Test
    public void testGirlGoodMoodWhenUglyAndBoyfriendRich() {
        boy = new Boy(Month.APRIL, 1000000);
        girl = new Girl(false,false, boy);
        assertEquals(girl.getMood(), Mood.GOOD);
    }

    @Test
    public void testGirlNeutralMoodWhenFriendFatWithoutBoyfriend() {
        assertEquals(girl.getMood(), Mood.NEUTRAL);
    }

    @Test
    public void testGirlNeutralMoodWhenFriendFatAndBoyfriendPoor() {
        girl = new Girl(false, true, boy);
        assertEquals(girl.getMood(), Mood.NEUTRAL);
    }

    @Test
    public void testUglyGirlHateMoodWhenBoyfriendPoor() {
        girl = new Girl(false,false, boy);
        assertEquals(girl.getMood(), Mood.I_HATE_THEM_ALL);
    }

    @Test
    public void testGirlHateMoodWhenFriendSlimAndWithoutBoyfriend() {
        girl = new Girl(false, false);
        assertEquals(girl.getMood(), Mood.I_HATE_THEM_ALL);
    }

}