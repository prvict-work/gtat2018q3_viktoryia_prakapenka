package com.epam.gomel.homework.listeners;

import org.testng.*;

public class TestListener implements IInvokedMethodListener {

    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
        System.out.println("[METHOD_STARTED] - " + method.getTestMethod().getMethodName());
    }

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
        System.out.println(String.format("[METHOD_FINISHED] - %s >>> %s",
                method.getTestMethod().getMethodName(), testResult.getStatus() == 1 ? "Success" : "Fail"));

    }
}